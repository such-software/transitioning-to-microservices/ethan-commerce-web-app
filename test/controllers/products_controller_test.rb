require "test_helper"

class ProductsControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get products_url
    assert_response :success
  end

  test "should get new" do
    get new_product_url
    assert_response :success
  end

  test "should create product" do
    product = EthanCommerce::Controls::Product.example

    # post products_url, params: { product: { description: @product.description, price: @product.price, quantity: @product.quantity, title: @product.title } }
    post products_url, params: { product: { title: product.title, description: product.description, price: product.price, quantity: product.quantity } }

    product = @controller.view_assigns['product']

    product_entry = ProductEntry.find(product.id)
    assert product_entry
    assert product_entry.title = product.title
    assert product_entry.description = product.description

    product_price = ProductPrice.find(product.id)
    assert product_price
    assert product_price.price = product.price

    product_inventory = ProductInventory.find(product.id)
    assert product_inventory
    assert product_inventory.quantity = product.quantity

    assert_redirected_to product_url(product)
  end

  test "should show product" do
    product = EthanCommerce::Controls::Product.example
    product.save!

    get product_url(product)
    assert_response :success
  end

  test "should get edit" do
    product = EthanCommerce::Controls::Product.example
    product.save!

    get edit_product_url(product)
    assert_response :success
  end

  test "should update product" do
    product = EthanCommerce::Controls::Product.example
    product.save!

    new_title = "new title"
    new_description = "new description"
    new_price = "123.00"
    new_quantity = 42

    patch product_url(product), params: { product: { title: new_title, description: new_description, price: new_price, quantity: new_quantity } }

    new_product = Product.find(product.id)
    assert new_product.title == new_title
    assert new_product.description == new_description
    assert new_product.price == new_price
    assert new_product.quantity == new_quantity


    assert_redirected_to product_url(product)
  end

  test "should destroy product" do
    product = EthanCommerce::Controls::Product.example
    product.save!

    delete product_url(product)

    retrieved_product = Product.find(product.id)
    assert retrieved_product.nil?

    assert_redirected_to products_url
  end
end

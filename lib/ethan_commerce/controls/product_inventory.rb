module EthanCommerce
  module Controls
    module ProductInventory
      def self.example
        ::ProductInventory.new(quantity: 1)
      end
    end
  end
end

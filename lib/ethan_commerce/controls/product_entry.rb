module EthanCommerce
  module Controls
    module ProductEntry
      def self.example
        ::ProductEntry.new(title:, description:)
      end

      def self.title
        "WaySus Gaming Chiar Ergonomic PC Video Game Chair Racing Computer Chair with Lumbar Support Flip Up Arms Headrest PU Leather Executive High Back Chair"
      end

      def self.description
        "The kind of chair people buy for husbands and that husbands love."
      end
    end
  end
end

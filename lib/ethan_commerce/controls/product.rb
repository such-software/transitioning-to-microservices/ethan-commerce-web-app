module EthanCommerce
  module Controls
    module Product
      def self.example
        product_entry = ProductEntry.example
        product_price = ProductPrice.example
        product_inventory = ProductInventory.example

        product = ::Product.new

        product.title = product_entry.title
        product.description = product_entry.description
        product.price = product_price.price
        product.quantity = product_inventory.quantity

        product
      end
    end
  end
end

module EthanCommerce
  module Controls
    module ProductPrice
      def self.example
        ::ProductPrice.new(price: "1.50")
      end
    end
  end
end

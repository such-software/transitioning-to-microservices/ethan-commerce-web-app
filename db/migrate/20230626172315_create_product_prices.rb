class CreateProductPrices < ActiveRecord::Migration[7.0]
  def change
    create_table :product_prices, primary_key: :product_id do |t|
      t.string :price

      t.timestamps
    end
  end
end

class CreateProductEntries < ActiveRecord::Migration[7.0]
  def change
    create_table :product_entries, primary_key: :product_id do |t|
      t.string :title
      t.string :description

      t.timestamps
    end
  end
end

class Product
  include ActiveModel::API
  include ActiveModel::Conversion

  attr_accessor :id
  attr_accessor :title
  attr_accessor :description
  attr_accessor :price
  attr_accessor :quantity

  attr_accessor :persisted

  ALL_QUERY = <<-SQL
    SELECT pe.product_id AS id, title, description, price, quantity
    FROM product_entries AS pe
      FULL OUTER JOIN product_prices AS pp
        ON pe.product_id = pp.product_id
      FULL OUTER JOIN product_inventories AS pi
        ON pe.product_id = pi.product_id
  SQL

  # TODO: Come back to this.  It works for now, but I'm not sure how it shows up in the UI
  def self.all
    ActiveRecord::Base.connection.exec_query(ALL_QUERY).map do |row|
      build(row)
    end
  end

  FIND_QUERY = <<-SQL
    SELECT pe.product_id AS id, title, description, price, quantity
    FROM product_entries AS pe
      FULL OUTER JOIN product_prices AS pp
        ON pe.product_id = pp.product_id
      FULL OUTER JOIN product_inventories AS pi
        ON pe.product_id = pi.product_id
    WHERE pe.product_id = $1
  SQL

  def self.find(product_id)
    rows = ActiveRecord::Base.connection.exec_query(FIND_QUERY, "Find product", [product_id])

    return nil if rows.first.nil?

    build(rows.first)
  end

  def self.build(row)
    product = Product.new(row)

    product.persisted = true

    product
  end

  def persisted?
    persisted
  end

  def update(params)
    self.title = params["title"]
    self.description = params["description"]
    self.price = params["price"]
    self.quantity = params["quantity"]

    ActiveRecord::Base.transaction do
      ProductEntry.update(id, title:, description:)
      ProductPrice.update(id, price:)
      ProductInventory.update(id, quantity:)
    end
  end

  def save!
    product_entry = ProductEntry.new(product_id: id, title:, description:)
    product_price = ProductPrice.new(product_id: id, price:)
    product_inventory = ProductInventory.new(product_id: id, quantity:)

    ActiveRecord::Base.transaction do
      product_entry.save!
      product_price.save!
      product_inventory.save!
    end

    self.id = product_entry.id

    self.persisted = true
  end

  def destroy
    product_entry = ProductEntry.find(id)
    product_price = ProductPrice.find(id)
    product_inventory = ProductInventory.find(id)

    ActiveRecord::Base.transaction do
      product_entry.destroy!
      product_price.destroy!
      product_inventory.destroy!
    end

    self.persisted = false
  end
end
